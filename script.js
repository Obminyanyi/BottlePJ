var rotateImg = document.getElementById("rotateObj");
var btn = document.getElementById("btn");

function getRandom(min, max) {
    return Math.floor(Math.random() * (min - max + 1)) + max;
}

function rotate() {
    var angle = 0;

    return function(e) {

        var cAngle = angle;
        btn.disabled = true;

        angle = getRandom(angle, angle + 360 * 10);
        var nTurn = (angle - cAngle) / 360;
        rotateImg.style.transitionDuration = nTurn + 's';
        rotateImg.style.transform = 'rotate(' + angle + 'deg)';
        setTimeout(function(func) {
            btn.disabled = false; 
        }, nTurn * 1000);
    }
}


//_____ users position___________________


btn.onclick = rotate();

function userPosition() {
var users = document.getElementsByClassName('user');
var delta = Math.PI * 2 / users.length;
var x = 0,
    y = 0,
    angle = 0;

for (var i = 0; i < users.length; i++) {
    users[i].style.position = 'absolute';
    users[i].style.left = 220 * Math.cos(angle) + 'px';
    users[i].style.top = 220 * Math.sin(angle) + 'px';
    angle += delta;
}
}

//=====================================================

var sC = 'user'; // selector class name

document.addEventListener('DOMContentLoaded', function(){
    var add = document.getElementById('addSector');
    var holder = document.getElementsByClassName('main-circle')[0];

    add.addEventListener('click', function(){
        var cSector = document.createElement('img'); // current sector
        cSector.classList.add(sC);
        cSector.src = 'user.png';
         holder.appendChild(cSector);
         userPosition();
    });
});